let http = require('http');

const port= 4000;

//Mock Databse
let directory = [
	{"name":"Brandon","email":"brandon@gmail.com"},
	{"name":"Jobert","email":"jobert@gmail.com"}
];

const server = http.createServer((request,response)=>{
	if(request.url == '/users' && request.method == 'GET'){
		//sets response output to JSON data type
		response.writeHead(200,{'Content-Type':'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}
	//Route for creating data
	if(request.url == '/users' && request.method == 'POST'){
		let requestBody = '';
		request.on('data',(data)=>{
			requestBody += data;
		});

		// ends when request is sent
		request.on('end',()=>{

			// Check data type of requestBody  if SRING
			console.log(typeof(requestBody));

			//converts string requestbody to JSON
			requestBody = JSON.parse(requestBody);

			// create new object for new mock database
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			// Add new user to mockDatabase
			directory.push(newUser);

			//check content of directory
			console.log(directory);
			response.writeHead(200,{'Content-Type': "application/json"});
			response.write(JSON.stringify(newUser));
			response.end();
		});

	}
});

server.listen(port);
console.log(`Server is running in localhost: ${port}`);