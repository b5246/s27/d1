let http = require("http");
const port = 4000;
const server = http.createServer((request,response)=>{
	//GET
	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Data retrieved from the database');}
	// POST
	if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end('Data to be sent to the database');}


});
server.listen(port);
console.log(`Server running at localhost: ${port}`);
